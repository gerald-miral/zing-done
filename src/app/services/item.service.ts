import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Item } from '../models/item.model';
import { BehaviorSubject, Observable, Subject, tap } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ItemService {

  items$ = new Subject<Item[]>();

  constructor(private httpClient: HttpClient) {
  }

  getAll(): Observable<Item[]> {
    return this.httpClient.get<Item[]>(`${environment.backendApi}/items`)
      .pipe(
        tap(items => {
          this.items$.next(items)
        })
      )
  }

  find(id: string) {
    return this.httpClient.get<Item>(`${environment.backendApi}/items/${id}`);
  }
}
