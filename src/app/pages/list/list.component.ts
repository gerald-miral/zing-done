import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Item } from 'src/app/models/item.model';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  items$: Observable<Item[]>

  constructor(private itemService: ItemService, private router: Router) {
    this.items$ = this.itemService.items$;
  }

  viewItem(id: string) {
    this.router.navigate(['list/item'], { queryParams: { id: id } });
  }
}
