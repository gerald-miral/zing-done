import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Item } from 'src/app/models/item.model';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  item$!: Observable<Item>

  constructor(private itemService: ItemService, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      const id = params['id']
      if (id) {
        this.item$ = this.itemService.find(id);
      }
    });
  }

  ngOnInit(): void {
  }

}
